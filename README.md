# gRPC-web Vue Counter
DOU.ua [gRPC-автогенерація Front-end-у](https://dou.ua/lenta/articles/grpc-web-guide/)

# Sizes:
* Only gRPC app ~ 282 KB
* With Vue.js ~ 350 KB

# For run see Makefile
```bash
go run main.go
```
