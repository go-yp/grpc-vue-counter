/**
 * @fileoverview gRPC-Web generated client stub for counter
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.counter = require('./counter_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.counter.CounterClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.counter.CounterPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.counter.Empty,
 *   !proto.counter.Response>}
 */
const methodDescriptor_Counter_Increment = new grpc.web.MethodDescriptor(
  '/counter.Counter/Increment',
  grpc.web.MethodType.UNARY,
  proto.counter.Empty,
  proto.counter.Response,
  /**
   * @param {!proto.counter.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.counter.Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.counter.Empty,
 *   !proto.counter.Response>}
 */
const methodInfo_Counter_Increment = new grpc.web.AbstractClientBase.MethodInfo(
  proto.counter.Response,
  /**
   * @param {!proto.counter.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.counter.Response.deserializeBinary
);


/**
 * @param {!proto.counter.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.counter.Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.counter.Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.counter.CounterClient.prototype.increment =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/counter.Counter/Increment',
      request,
      metadata || {},
      methodDescriptor_Counter_Increment,
      callback);
};


/**
 * @param {!proto.counter.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.counter.Response>}
 *     A native promise that resolves to the response
 */
proto.counter.CounterPromiseClient.prototype.increment =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/counter.Counter/Increment',
      request,
      metadata || {},
      methodDescriptor_Counter_Increment);
};


module.exports = proto.counter;

