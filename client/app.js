const {Vue} = require("vue");
const {Empty, Response} = require("./protos/services/counter/counter_pb");
const {CounterClient} = require("./protos/services/counter/counter_grpc_web_pb");

const app = new CounterClient("http://localhost:50551");

var request = new Empty();

app.increment(request, {}, (err, response) => {
    if (err) {
        console.error(err);

        return;
    }

    /** @type Response response */

    document.getElementById("js-counter").innerHTML = response.getCount();

    const app = new Vue({
        el: '#js-counter',
        data: {
            count: response.getCount(),
        }
    });
});
